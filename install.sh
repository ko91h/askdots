#!/bin/sh

sudo apt-get update  -y
sudo apt-get install -y vim-nox
sudo apt-get install -y git
sudo apt-get install -y ctags
sudo apt-get install -y silversearcher-ag

sudo apt-get install -y build-essential cmake
sudo apt-get install -y python-dev python3-dev

# vim
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# nvim
# curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
#    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

mkdir -p ~/.vim/undo
ln -s $(pwd)/vimrc ~/.vimrc
vim -u ~/.vimrc +PlugInstall +qall
