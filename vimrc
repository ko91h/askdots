"=============================================================================
" Description: neovim init.vim
" Author: Nikolay Meleshenko <ko91h7@gmail.com>
" URL:
"=============================================================================
    if !has('nvim')
        set nocompatible
    endif
    " vim-plug section
    call plug#begin()

    " IDE
        Plug 'majutsushi/tagbar'
        Plug 'altercation/vim-colors-solarized'

        Plug 'jlanzarotta/bufexplorer'

        Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeFind' }
        Plug 'scrooloose/nerdcommenter'

        Plug 'godlygeek/tabular'

        Plug 'tpope/vim-sensible'
        Plug 'tpope/vim-surround'
        Plug 'tpope/vim-repeat'
        Plug 'tpope/vim-fugitive'
        Plug 'airblade/vim-gitgutter'
        Plug 'ctrlpvim/ctrlp.vim'

        Plug 'edsono/vim-matchit'
        Plug 'vim-airline/vim-airline'
        Plug 'vim-airline/vim-airline-themes'

        Plug 'bronson/vim-trailing-whitespace'

        Plug 'sjl/gundo.vim', { 'on': 'GundoToggle' }
        Plug 'Valloric/YouCompleteMe', { 'do': './install.py', }
        " YCM old centos workaround
        " Plug 'Valloric/YouCompleteMe', { 'do': './install.py', 'commit': '0de1c0c9bb13ce82172b472c676035cd47cf6a6a' }
    " HTML/HAML
        Plug 'othree/html5.vim', { 'for' : 'html' }
        Plug 'mattn/emmet-vim', { 'for' : ['html','css','scss'] }
    " CSS/LESS
        Plug 'hail2u/vim-css3-syntax', { 'for' : ['css','scss'] }
        Plug 'groenewege/vim-less', { 'for' : 'less' }
    " JavaScript
        Plug 'pangloss/vim-javascript', { 'for' : 'javascript' }
        Plug 'itspriddle/vim-jquery', { 'for' : 'javascript' }
    " JSON
        Plug 'leshill/vim-json', { 'for' : 'json' }
    " Nginx
        Plug 'nginx.vim', { 'for' : 'nginx' }
    " Python syntax
        Plug 'hdima/python-syntax', { 'for' : 'python' }
        Plug 'django.vim', { 'for' : 'htmldjango' }
        Plug 'nvie/vim-flake8', { 'for' : 'python' }
        Plug 'Jinja'
    " Ag
        Plug 'rking/ag.vim', { 'on' : 'Ag' }

    call plug#end()

        set nowrap
        let loaded_matchparen=1 " перестает прыгать на парную скобку, показывая где она. +100 к скорости

        set linebreak           " Перенос не разрывая слов
        set autoindent          " Копирует отступ от предыдущей строки
        set smartindent         " Включаем 'умную' автоматическую расстановку отступов
        set expandtab
        set smarttab            " вставляет табуляцию в начале строки размера shiftwidth
        set shiftwidth=4        " Размер сдвига при нажатии на клавиши << и >>
        set tabstop=4           " Размер табуляции
        set softtabstop=4

    " Search
        set incsearch   " При поиске перескакивать на найденный текст в процессе набора строки
        set hlsearch    " Включаем подсветку выражения, которое ищется в тексте
        set ignorecase  " Игнорировать регистр букв при поиске
        set smartcase   " Override the 'ignorecase' if the search pattern contains upper case characters
        set gdefault    " Включает флаг g в командах замены, типа :%s/a/b/

        set isfname+=-
        set path=.,,,

    " undo
        set undofile
        if has('nvim')
            set undodir=~/.nvim/undo
        else
            set undodir=~/.vim/undo
        endif
        set undolevels=5000

        let mapleader = "," " мапим <Leader> на запятую. По умолчанию <Leader> это обратный слэш \

        set hidden " this allows to edit several files in the same time without having to save them

    " Редактирование конфига
        nmap <leader>v :edit $MYVIMRC<CR>
        if has("autocmd")
          autocmd! bufwritepost .vimrc source $MYVIMRC
        endif

    " Переназначение ESC
        inoremap jj <ESC>

    " buffers navigation
    " To open a new empty buffer
    " This replaces :tabnew which I used to bind to this mapping
        nmap <leader>T :enew<cr>

    " Move to the next buffer
        nmap <leader>l :bnext!<CR>

    " Move to the previous buffer
        nmap <leader>h :bprevious!<CR>

    " Close the current buffer and move to the previous one
    " This replicates the idea of closing a tab
        nmap <leader>bq :bp <BAR> bd #<CR>

    " ,пробел выключает подсветку результатов поиска
        map <leader><space> :noh<CR>

    " Пробел перелистывает страницы
        nmap <Space> <PageDown>

    " leader short keys
        map <leader>tt :TagbarToggle<CR>
        map <leader>tf :NERDTreeFind <CR>
        map <leader>bb :BufExplorer<CR>

    " ,p
        " Вставлять код извне без этой строчки проблематично, без нее начитается
        " бешеный реформат кода
        set pastetoggle=<Leader>p

    " ,nm
        " Toggle type of line numbers
        let g:relativenumber = 0
        function! ToogleRelativeNumber()
            if g:relativenumber == 0
                let g:relativenumber = 1
                set norelativenumber
                set number
                echo "Show line numbers"
            elseif g:relativenumber == 1
                let g:relativenumber = 2
                set nonumber
                set relativenumber
                echo "Show relative line numbers"
            else
                let g:relativenumber = 0
                set nonumber
                set norelativenumber
                echo "Show no line numbers"
            endif
        endfunction
        map <Leader>nm :call ToogleRelativeNumber()<cr>

    " S - Silence
        set noerrorbells
        set novisualbell
        set t_vb=

    " Don't lose selection when shifting sidewards
        vnoremap < <gv
        vnoremap > >gv

    " Переключение по сплитам
        nmap <C-h> <C-W>h
        nmap <C-j> <C-W>j
        nmap <C-k> <C-W>k
        nmap <C-l> <C-W>l

    " Don't skip wrap lines
        noremap j gj
        noremap k gk

    " ,u Change case to uppercase
        nnoremap <Leader>u gUiw
        inoremap <Leader>u <esc>gUiwea

    " В коммандном режиме разрешить прыгать в конец и начало строки,
    " как в консоли
        cnoremap <c-e> <end>
        imap     <c-e> <c-o>$
        cnoremap <c-a> <home>
        imap     <c-a> <c-o>^

    " n и N
    " когда бегаем по результатам поиска, то пусть они всегда будут в центре
        nmap n nzz
        nmap N Nzz
        nmap * *zz
        nmap # #zz
        nmap g* g*zz
        nmap g# g#zz

    " ,ts fix trailing spaces
        map <silent> <leader>ts :FixWhitespace<CR>

    " Filetypes

        au BufRead,BufNewFile,BufWritePost *.html if  search('{{') > 0  || search('{%') > 0 | set filetype=htmldjango | endif
        au BufRead,BufNewFile *.wsgi set filetype=python
        au BufRead,BufNewFile *nginx*conf* set filetype=nginx
        au BufRead,BufNewFile .envrc set filetype=sh

    " Plugins settings

    " Airline {{{
        let g:airline_theme='bubblegum'
        let g:airline_powerline_fonts = 1
        let g:airline_section_b='%{getcwd()}'
        let g:airline#extensions#whitespace#checks = [ 'trailing' ]
        let g:airline#extensions#ctrlp#color_template = 'normal'
        let g:airline#extensions#ctrlp#show_adjacent_modes = 1
        let g:airline#extensions#tabline#enabled = 1
        let g:airline#extensions#tabline#fnamemod = ':t'
    " }}}
    " gundo {{{
        let g:gundo_preview_bottom = 1
        let g:gundo_right = 1
        nnoremap <silent>tg :GundoToggle<CR>
    " }}}
    " python syntax {{{
        let python_highlight_all=1
    " }}}
    " CtrlP {{{
        let g:ctrlp_open_new_file = 't'
        let g:ctrlp_follow_symlinks = 2
        let g:ctrlp_clear_cache_on_exit = 0
        let g:ctrlp_prompt_mappings = {
            \   'AcceptSelection("e")': ['<c-m>'],
            \   'PrtSelectMove("j")':   ['<c-j>'],
            \   'PrtSelectMove("k")':   ['<c-k>'],
            \   'PrtHistory(-1)':       ['<down>'],
            \   'PrtHistory(1)':        ['<up>'],
        \   }
        let g:ctrlp_root_markers = ['.git']
    " }}}
    " The Silver Searcher {{{
        if executable('ag')
            " Use ag over grep
            set wildignore+=*/data/js/*,*/data/css/*,*/.git/*,*/migrations/*.py
            set grepprg=ag\ --nogroup\ --nocolor\ --column
            set grepformat=%f:%l:%c:%m

            let g:ctrlp_use_caching = 0

            let g:agprg='ag --nogroup --column '

            let g:ctrlp_user_command = 'ag %s -l --nocolor '
            let g:ag_format="%f:%l:%c:%m"
            let g:ctrlp_user_command .= ' -ig ""'

            map <Leader>x :Ag "\b<C-R><C-W>\b" $AG_PATH <CR>:cw<CR>
            map <Leader>z :Ag "<C-R><C-W>" $AG_PATH <CR>:cw<CR>
        endif
    " }}}
    " NERDTree {{{
        let NERDTreeShowBookmarks=1
        let NERDTreeChDirMode=2
        let NERDTreeQuitOnOpen=1
        let NERDTreeShowHidden=0
        let NERDTreeKeepTreeInNewTab=0
        let NERDTreeMinimalUI=1 " Disables display of the 'Bookmarks' label and 'Press ? for help' text.
        let NERDTreeDirArrows=1 " Tells the NERD tree to use arrows instead of + ~ chars when displaying directories.
        let NERDTreeBookmarksFile= $HOME . '/.vim/.NERDTreeBookmarks'
        let NERDTreeWinSize=60
        let NERDTreeIgnore=['\.pyc$', '\.pyo$']
    " }}}
    " Solarized {{{
        syntax enable
        set background=dark
        let g:solarized_termcolors=16
        let g:solarized_termtrans=1
        let g:solarized_degrade=0
        let g:solarized_bold=0
        let g:solarized_underline=1
        let g:solarized_italic=1
        let g:solarized_termcolors=16
        let g:solarized_contrast="normal"
        let g:solarized_visibility="normal"
        let g:solarized_diffmode="normal"
        let g:solarized_hitrail=0
        let g:solarized_menu=1
        let g:my_colorscheme='solarized'
        try
            execute 'colorscheme' g:my_colorscheme
        catch /^Vim\%((\a\+)\)\=:E185/
            echo g:my_colorscheme . " theme not found. Run :PlugInstall"
        endtry
    " }}}
    " gitgutter {{{
        nmap gj <Plug>GitGutterNextHunk
        nmap gk <Plug>GitGutterPrevHunk
    " }}}
